# Brolas st
My personal fork of Suckless's st.

## Patches
Patches may be found in patch/, but the major ones are as follows:

- Alpha
- Ligature support
- Newterm
- Scrollback

## Colorscheme and transparency
The vast majority of colors, with the exception of black are plucked from [vim-deus](https://github.com/ajmwagar/vim-deus).
Any composite manager should enable transparency. By default focused and unfocused windows are 90% and 75% transparent respectively.

## Preview
Screenshots of the terminal window, with color showcase and sample python code:
![Colors](screenshots/st.png)
![Python](screenshots/st_code.png)

## Installation
Clone this directory and run

    make clean install

as root. It should be noted that the Makefile is configured to overwrite `config.h` with `config.def.h`. While potentially unsafe, it is my preference when patching. 
